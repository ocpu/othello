declare namespace firebase {
    interface Thenable<T> {
        /**
         * Assign a callback when the Thenable rejects.
         * @param onReject Called when the Thenable is rejected (with an error).
         */
        catch(onReject?: (error: Error) => any): firebase.Thenable<any>
        /**
         * Assign callback functions called when the Thenable value either resolves, or is rejected.
         * @param onResolve Called when the Thenable resolves.
         * @param onReject Called when the Thenable is rejected (with an error).
         */
        then(onResolve?: (value: T) => any, onReject?: (error: Error) => any): firebase.Thenable<any>
    }
    interface FirebaseError extends Error {
        code: string
        message: string
        name: string
        stack: string
    }
    interface User extends UserInfo {
        /**
         * True if the user's email address has been verified.
         */
        emailVerified: boolean
        /**
         * If the user is anonymous
         */
        isAnonymous: boolean
        /**
         * The phone number normalized based on the E.164 standard (e.g. +16505550101) for the current user. This is null if the user has no phone credential linked to the account.
         */
        phoneNumber?: string
        /**
         * Additional provider-specific information about the user.
         */
        providerData: firebase.UserInfo[]
        /**
         * A refresh token for the user account. Use only for advanced scenarios that require explicitly refreshing tokens.
         */
        refreshToken: string

        /**
         * Deletes and signs out the user.
         * 
         * Important: this is a security sensitive operation that requires the user to have recently signed in. If this requirement isn't met, ask the user to authenticate again and then call firebase.User#reauthenticateWithCredential.
         */
        delete(): firebase.Promise<void>
        /**
         * Returns a JWT token used to identify the user to a Firebase service.
         * 
         * Returns the current token if it has not expired, otherwise this will refresh the token and return a new one.
         * @param forceRefresh Force refresh regardless of token expiration.
         */
        getIdToken(forceRefresh?: boolean): firebase.Promise<string>
        /**
         * Links the user account with the given credentials, and returns any available additional user information, such as user name.
         * @param credential The auth credential
         */
        linkAndRetrieveDataWithCredential(credential: firebase.auth.AuthCredential): firebase.Promise<firebase.auth.AuthCredential>
        /**
         * Links the user account with the given credentials.
         * @param credential The auth credential
         */
        linkWithCredential(credential: firebase.auth.AuthCredential): firebase.Promise<firebase.User>
        /**
         * Links the user account with the given credentials.
         * @param phoneNumber The user's phone number in E.164 format (e.g. +16505550101).
         */
        linkWithPhoneNumber(phoneNumber: string, applicationVerifier: firebase.auth.ApplicationVerifier): firebase.Promise<firebase.auth.ConfirmationResult>
        /**
         * Links the authenticated provider to the user account using a pop-up based OAuth flow.
         *
         * If the linking is successful, the returned result will contain the user and the provider's credential.
         * @param provider The provider to authenticate. The provider has to be an OAuth provider. Non-OAuth providers like firebase.auth.EmailAuthProvider will throw an error.
         */
        linkWithPopup(provider: firebase.auth.AuthProvider): firebase.Promise<firebase.auth.UserCredential>
        /**
         * Links the authenticated provider to the user account using a full-page redirect flow.
         * @param provider The provider to authenticate. The provider has to be an OAuth provider. Non-OAuth providers like firebase.auth.EmailAuthProvider will throw an error.
         */
        linkWithRedirect(provider: firebase.auth.AuthProvider): firebase.Promise<void>
        /**
         * Re-authenticates a user using a fresh credential, and returns any available additional user information, such as user name. Use before operations such as firebase.User#updatePassword that require tokens from recent sign-in attempts.
         */
        reauthenticateAndRetrieveDataWithCredential(credential: firebase.auth.AuthCredential): firebase.Promise<void>
        /**
         * Re-authenticates a user using a fresh credential. Use before operations such as firebase.User#updatePassword that require tokens from recent sign-in attempts.
         */
        reauthenticateWithCredential(credential: firebase.auth.AuthCredential): firebase.Promise<void>
        /**
         * Re-authenticates a user using a fresh credential. Use before operations such as firebase.User#updatePassword that require tokens from recent sign-in attempts.
         * @param phoneNumber The user's phone number in E.164 format (e.g. +16505550101).
         */
        reauthenticateWithPhoneNumber(phoneNumber: string, applicationVerifier: firebase.auth.ApplicationVerifier): firebase.Promise<firebase.auth.ConfirmationResult>
        /**
         * Reauthenticates the current user with the specified provider using a pop-up based OAuth flow.
         *
         * If the reauthentication is successful, the returned result will contain the user and the provider's credential.
         * @param provider The provider to authenticate. The provider has to be an OAuth provider. Non-OAuth providers like firebase.auth.EmailAuthProvider will throw an error.
         */
        reauthenticateWithPopup(provider: firebase.auth.AuthProvider): firebase.Promise<firebase.auth.UserCredential>
        /**
         * Reauthenticates the current user with the specified OAuth provider using a full-page redirect flow.
         * @param provider The provider to authenticate. The provider has to be an OAuth provider. Non-OAuth providers like firebase.auth.EmailAuthProvider will throw an error.
         */
        reauthenticateWithRedirect(provider: firebase.auth.AuthProvider): firebase.Promise<void>
        /**
         * Refreshes the current user, if signed in.
         */
        reload(): firebase.Promise<void>
        /**
         * Sends a verification email to a user.
         *
         * The verification process is completed by calling firebase.auth.Auth#applyActionCode
         */
        sendEmailVerification(): firebase.Promise<void>
        /**
         * Returns a JSON-serializable representation of this object.
         */
        toJSON(): Object
        /**
         * Unlinks a provider from a user account.
         */
        unlink(providerId: string): firebase.Promise<firebase.User>
        /**
         * Updates the user's email address.
         *
         * An email will be sent to the original email address (if it was set) that allows to revoke the email address change, in order to protect them from account hijacking.
         *
         * Important: this is a security sensitive operation that requires the user to have recently signed in. If this requirement isn't met, ask the user to authenticate again and then call firebase.User#reauthenticateWithCredential.
         * @param newEmail The new email address.
         */
        updateEmail(newEmail: string): firebase.Promise<void>
        /**
         * Updates the user's password.
         *
         * Important: this is a security sensitive operation that requires the user to have recently signed in. If this requirement isn't met, ask the user to authenticate again and then call firebase.User#reauthenticateWithCredential.
         * @param newPassword The new password.
         */
        updatePassword(newPassword: string): firebase.Promise<void>
        /**
         * Updates the user's phone number.
         * @param newPassword The new password.
         */
        updatePhoneNumber(phoneCredential: firebase.auth.AuthCredential): firebase.Promise<void>
        /**
         * Updates a user's profile data.
         * @param profile The profile's displayName and photoURL to update.
         */
        updateProfile(profile: { displayName?: string, photoURL?: string }): firebase.Promise<void>
    }
    interface UserInfo {
        /** 
         * The user's display name (if available).
         */
        displayName?: string
        /**
         * The user's email address (if available).
         */
        email?: string
        /**
         * The URL of the user's profile picture (if available).
         */
        photoURL?: string
        /**
         * The authentication provider ID for the current user. For example, 'facebook.com', or 'google.com'.
         */
        providerId: string
        /**
         * The user's unique ID.
         */
        uid: string
    }
    interface Promise<T> extends firebase.Thenable<T> {
    }
    var Promise: {
        /**
         * A Promise represents an eventual (asynchronous) value. A Promise should (eventually) either resolve or reject. When it does, it will call all the callback functions that have been assigned via the .then() or .catch() methods.
         *
         * firebase.Promise is the same as the native Promise implementation when available in the current environment, otherwise it is a compatible implementation of the Promise/A+ spec.
         */
        new<T>(resolver: (resolve: (value: T) => void, reject: (error: Error) => void) => void): firebase.Promise<T>
        prototype: firebase.Promise<any>
        /**
         * Convert an array of Promises, to a single array of values. Promise.all() resolves only after all the Promises in the array have resolved.
         *
         * Promise.all() rejects when any of the promises in the Array have rejected.
         */
        all(values: firebase.Promise<any>[]): firebase.Promise<any[]>
        /**
         * Return (an immediately) rejected Promise.
         * @param error The reason for the Promise being rejected.
         */
        reject(error: Error): firebase.Promise<any>
        /**
         * Return a resolved Promise.
         * @param value The value to be returned by the Promise.
         */
        resolve<T>(value: T): firebase.Promise<T>
    }
    namespace app {
        interface App {
            /**
             * The (read-only) name for this app.
             * 
             * The default app's name is "[DEFAULT]".
             */
            readonly name: string
            /**
             * The (read-only) configuration options for this app. These are the original parameters given in firebase.initializeApp().
             */
            readonly options: Object

            /**
             * Gets the Auth service for the current app.
             */
            auth(): firebase.auth.Auth
            /**
             * Gets the Database service for the current app.
             */
            database(): firebase.database.Database
            /**
             * Renders this app unusable and frees the resources of all associated services.
             */
            delete(): firebase.Promise<void>
            /**
             * Gets the Messaging service for the current app.
             */
            messaging(): firebase.messaging.Messaging
            /**
             * Gets the Storage service for the current app, optionally initialized with a custom storage bucket.
             * @param url The gs:// url to your Firebase Storage Bucket. If not passed, uses the app's default Storage Bucket.
             */
            storage(url?: string): firebase.storage.Storage
        }
    }
    namespace auth {
        interface ActionCodeInfo {
            /**
             * The email address associated with the action code.
             */
            data: { email: string }
        }
        interface ApplicationVerifier {
            /**
             * Identifies the type of application verifier (e.g. "recaptcha").
             */
            type: string

            /**
             * Executes the verification process.
             * @returns A Promise for a token that can be used to assert the validity of a request.
             */
            verify(): firebase.Promise<string>
        }
        interface Auth {
            /**
             * The app associated with the <code>Auth</code> service instance.
             */
            app: firebase.app.App
            /**
             * The currently signed-in user (or null).
             */
            currentUser: firebase.User | null

            /**
             * Applies a verification code sent to the user by email or other out-of-band mechanism.
             * @param code A verification code sent to the user.
             */
            applyActionCode(code: string): firebase.Promise<void>
            /**
             * Checks a verification code sent to the user by email or other out-of-band mechanism.
             * 
             * Returns metadata about the code.
             * @param code A verification code sent to the user.
             */
            checkActionCode(code: string): firebase.Promise<firebase.auth.ActionCodeInfo>
            /**
             * Completes the password reset process, given a confirmation code and new password.
             * @param code The confirmation code send via email to the user.
             * @param newPassword The new password.
             */
            confirmPasswordReset(code: string, newPassword: string): firebase.Promise<void>
            /**
             * Creates a new user account associated with the specified email address and password.
             * 
             * On successful creation of the user account, this user will also be signed in to your application.
             * 
             * User account creation can fail if the account already exists or the password is invalid.
             * 
             * Note: The email address acts as a unique identifier for the user and enables an email-based password reset. This function will create a new user account and set the initial user password.
             * @param email The user's email address.
             * @param password The user's chosen password.
             */
            createUserWithEmailAndPassword(email: string, password: string): firebase.Promise<firebase.User>
            /**
             * Gets the list of provider IDs that can be used to sign in for the given email address. Useful for an "identifier-first" sign-in flow.
             * @param email An email address
             */
            fetchProvidersForEmail(email: string): firebase.Promise<string[]>
            /**
             * Returns a UserCredential from the redirect-based sign-in flow.
             * 
             * If sign-in succeeded, returns the signed in user. If sign-in was unsuccessful, fails with an error. If no redirect operation was called, returns a UserCredential with a null User.
             */
            getRedirectResult(): firebase.Promise<firebase.auth.UserCredential>
            /**
             * Adds an observer for changes to the user's sign-in state.
             * 
             * Prior to 4.0.0, this triggered the observer when users were signed in, signed out, or when the user's ID token changed in situations such as token expiry or password change. After 4.0.0, the observer is only triggered on sign-in or sign-out.
             * 
             * To keep the old behavior, see firebase.auth.Auth#onIdTokenChanged.
             * @param nextOrObserver An observer object or a function triggered on change.
             * @param error A function triggered on auth error.
             * @param completed A function triggered when the observer is removed.
             */
            onAuthStateChange(nextOrObserver: Object | ((user?: firebase.User) => void), error?: (error: firebase.auth.Error) => void, completed?: Function): Function
            /**
             * Adds an observer for changes to the signed-in user's ID token, which includes sign-in, sign-out, and token refresh events. This method has the same behavior as firebase.auth.Auth#onAuthStateChanged had prior to 4.0.0.
             * @param nextOrObserver An observer object or a function triggered on change.
             * @param error A function triggered on auth error.
             * @param completed A function triggered when the observer is removed.
             */
            onIdTokenChanged(nextOrObserver: Object | ((user?: firebase.User) => void), error?: (error: firebase.auth.Error) => void, completed?: Function): Function
            /**
             * Sends a password reset email to the given email address.
             * 
             * To complete the password reset, call firebase.auth.Auth#confirmPasswordReset with the code supplied in the email sent to the user, along with the new password specified by the user.
             * @param email The email address with the password to be reset.
             */
            sendPasswordResetEmail(email: string): firebase.Promise<void>
            /**
             * Asynchronously signs in with the given credentials, and returns any available additional user information, such as user name.
             * @param credential The auth credential.
             */
            signInAndRetrieveDataWithCredential(credential: firebase.auth.AuthCredential): firebase.Promise<firebase.auth.UserCredential>
            /**
             * Asynchronously signs in as an anonymous user.
             * 
             * If there is already an anonymous user signed in, that user will be returned; otherwise, a new anonymous user identity will be created and returned.
             */
            signInAnonymously(): firebase.Promise<firebase.User>
            /**
             * Asynchronously signs in with the given credentials.
             * @param credential The auth credential.
             */
            signInWithCredential(credential: firebase.auth.AuthCredential): firebase.Promise<firebase.User>
            /**
             * Asynchronously signs in using a custom token.
             * 
             * Custom tokens are used to integrate Firebase Auth with existing auth systems, and must be generated by the auth backend.
             * 
             * Fails with an error if the token is invalid, expired, or not accepted by the Firebase Auth service.
             * @param token The custom token to sign in with.
             */
            signInWithCustomToken(token: string): firebase.Promise<firebase.User>
            /**
             * Asynchronously signs in using an email and password.
             * 
             * Fails with an error if the email address and password do not match.
             * 
             * Note: The user's password is NOT the password used to access the user's email account. The email address serves as a unique identifier for the user, and the password is used to access the user's account in your Firebase project.
             * @param email The users email address.
             * @param password The users password.
             */
            signInWithEmailAndPassword(email: string, password: string): firebase.Promise<firebase.User>
            /**
             * Asynchronously signs in using a phone number. This method sends a code via SMS to the given phone number, and returns a firebase.auth.ConfirmationResult. After the user provides the code sent to their phone, call firebase.auth.ConfirmationResult#confirm with the code to sign the user in.
             * 
             * For abuse prevention, this method also requires a firebase.auth.ApplicationVerifier. The Firebase Auth SDK includes a reCAPTCHA-based implementation, firebase.auth.RecaptchaVerifier.
             */
            signInWithPhoneNumber(phoneNumber: string, applicationVerifier: firebase.auth.ApplicationVerifier): firebase.Promise<firebase.auth.ConfirmationResult>
            /**
             * Authenticates a Firebase client using a popup-based OAuth authentication flow.
             * 
             * If succeeds, returns the signed in user along with the provider's credential. If sign in was unsuccessful, returns an error object containing additional information about the error.
             * @param provider The provider to authenticate. The provider has to be an OAuth provider. Non-OAuth providers like firebase.auth.EmailAuthProvider will throw an error.
             */
            signInWithPopup(provider: firebase.auth.AuthProvider): firebase.Promise<firebase.auth.UserCredential>
            /**
             * Authenticates a Firebase client using a full-page redirect flow. To handle the results and errors for this operation, refer to firebase.auth.Auth#getRedirectResult.
             * @param provider The provider to authenticate. The provider has to be an OAuth provider. Non-OAuth providers like firebase.auth.EmailAuthProvider will throw an error.
             */
            signInWithRedirect(provider: firebase.auth.AuthProvider): firebase.Promise<void>
            /**
             * Signs out the current user.
             */
            signOut(): firebase.Promise<void>
            /**
             * Checks a password reset code sent to the user by email or other out-of-band mechanism.
             * 
             * Returns the user's email address if valid.
             * @param code A verification code sent to the user.
             */
            verifyPasswordResetCode(code: string): firebase.Promise<string>
        }
        interface AuthCredential {
            /**
             * The authentication provider ID for the credential. For example, 'facebook.com', or 'google.com'.
             */
            provider: string
        }
        interface AuthProvider {
            providerId: string
        }
        interface ConfirmationResult {
            /**
             * The phone number authentication operation's verification ID. This can be used along with the verification code to initialize a phone auth credential.
             */
            verificationId: string

            /**
             * Finishes a phone number sign-in, link, or reauthentication, given the code that was sent to the user's mobile device.
             */
            confirm(verificationCode: string): firebase.Promise<firebase.auth.UserCredential>
        }
        interface Error {
            /**
             * Unique error code.
             */
            code: string
            /**
             * Complete error message.
             */
            message: string
        }
        interface EmailAuthProvider extends AuthProvider {
        }
        interface FacebookAuthProvider extends AuthProvider {
            /**
             * @param token Facebook OAuth scope.
             */
            addScope(scope: string): firebase.auth.AuthProvider
            /**
             * Sets the OAuth custom parameters to pass in a Facebook OAuth request for popup and redirect sign-in operations. Valid parameters include 'auth_type', 'display' and 'locale'. For a detailed list, check the Facebook documentation. Reserved required OAuth 2.0 parameters such as 'client_id', 'redirect_uri', 'scope', 'response_type' and 'state' are not allowed and will be ignored.
             * @param customOAuthParameters The custom OAuth parameters to pass in the OAuth request.
             */
            setCustomParameters(customOAuthParameters: { auth_type?: "rerequest" | "reauthenticate", display?: string, locale?: string }): firebase.auth.AuthProvider
        }
        interface GithubAuthProvider extends AuthProvider {
            /**
             * @param token Github OAuth scope.
             */
            addScope(scope: string): firebase.auth.AuthProvider
            /**
             * Sets the OAuth custom parameters to pass in a GitHub OAuth request for popup and redirect sign-in operations. Valid parameters include 'allow_signup'. For a detailed list, check the GitHub documentation. Reserved required OAuth 2.0 parameters such as 'client_id', 'redirect_uri', 'scope', 'response_type' and 'state' are not allowed and will be ignored.
             * @param customOAuthParameters The custom OAuth parameters to pass in the OAuth request.
             */
            setCustomParameters(customOAuthParameters: { allow_signup?: string }): firebase.auth.AuthProvider
        }
        interface GoogleAuthProvider extends AuthProvider {
            /**
             * @param token Google OAuth scope.
             */
            addScope(scope: string): firebase.auth.AuthProvider
            /**
             * Sets the OAuth custom parameters to pass in a Google OAuth request for popup and redirect sign-in operations. Valid parameters include 'hd', 'hl', 'include_granted_scopes', 'login_hint' and 'prompt'. For a detailed list, check the Google documentation. Reserved required OAuth 2.0 parameters such as 'client_id', 'redirect_uri', 'scope', 'response_type' and 'state' are not allowed and will be ignored.
             * @param customOAuthParameters The custom OAuth parameters to pass in the OAuth request.
             */
            setCustomParameters(customOAuthParameters: { hd?: string, hl?: string, include_granted_scopes?: boolean, login_hint?: string, prompt?: "none" | "consent" | "select_account" }): firebase.auth.AuthProvider
        }
        interface PhoneAuthProvider extends AuthProvider {
        }
        interface RecaptchaVerifier extends ApplicationVerifier {
            /**
             * Clears the reCAPTCHA widget from the page and destroys the current instance.
             */
            clear(): void
            /**
             * Renders the reCAPTCHA widget on the page.
             */
            render(): firebase.Promise<number>
        }
        interface TwitterAuthProvider extends AuthProvider {
            /**
             * Sets the OAuth custom parameters to pass in a Twitter OAuth request for popup and redirect sign-in operations. Valid parameters include 'lang'. Reserved required OAuth 1.0 parameters such as 'oauth_consumer_key', 'oauth_token', 'oauth_signature', etc are not allowed and will be ignored.
             * @param customOAuthParameters The custom OAuth parameters to pass in the OAuth request.
             */
            setCustomParameters(customOAuthParameters: { lang?: string }): firebase.auth.AuthProvider
        }
        var EmailAuthProvider: {
            new(): EmailAuthProvider
            prototype: EmailAuthProvider

            PROVIDER_ID: string

            /**
             * @param email Email address.
             * @param password User account password.
             */
            credential(email: string, password: string): firebase.auth.AuthCredential
        }
        var FacebookAuthProvider: {
            new(): FacebookAuthProvider
            prototype: FacebookAuthProvider

            PROVIDER_ID: string

            /**
             * @param token Facebook access token.
             */
            credential(token: string): firebase.auth.AuthCredential
        }
        var GithubAuthProvider: {
            new(): GithubAuthProvider
            prototype: GithubAuthProvider

            PROVIDER_ID: string

            /**
             * @param token Github access token.
             */
            credential(token: string): firebase.auth.AuthCredential
        }
        var GoogleAuthProvider: {
            new(): GoogleAuthProvider
            prototype: GoogleAuthProvider

            PROVIDER_ID: string

            /**
             * Creates a credential for Google. At least one of ID token and access token is required.
             * @param idToken Google ID token.
             * @param accessToken Google access token.
             */
            credential(idToken?: string, accessToken?: string): firebase.auth.AuthCredential
        }
        var PhoneAuthProvider: {
            /**
             * Phone number auth provider.
             * @param auth The Firebase Auth instance in which sign-ins should occur. Uses the default Auth instance if unspecified.
             */
            new(auth?: firebase.auth.Auth): PhoneAuthProvider
            prototype: PhoneAuthProvider

            PROVIDER_ID: string

            /**
             * Creates a phone auth credential, given the verification ID from firebase.auth.PhoneAuthProvider#verifyPhoneNumber and the code that was sent to the user's mobile device.
             * @param verificationId The verification ID returned from firebase.auth.PhoneAuthProvider#verifyPhoneNumber.
             * @param verificationCode The verification code sent to the user's mobile device.
             */
            credential(verificationId: string, verificationCode: string): firebase.Promise<firebase.auth.AuthCredential>
            /**
             * Starts a phone number authentication flow by sending a verification code to the given phone number. Returns an ID that can be passed to firebase.auth.PhoneAuthProvider#credential to identify this flow.
             * 
             * For abuse prevention, this method also requires a firebase.auth.ApplicationVerifier. The Firebase Auth SDK includes a reCAPTCHA-based implementation, firebase.auth.RecaptchaVerifier.
             * @param phoneNumber The user's phone number in E.164 format (e.g. +16505550101).
             */
            verifyPhoneNumber(phoneNumber: string, applicationVerifier:	firebase.auth.ApplicationVerifier): firebase.Promise<string>
        }
        var RecaptchaVerifier: {
            /**
             * @param container The reCAPTCHA container parameter. This has different meaning depending on whether the reCAPTCHA is hidden or visible. For a visible reCAPTCHA the container must be empty. If a string is used, it has to correspond to an element ID. The corresponding element must also must be in the DOM at the time of initialization.
             * @param container The optional reCAPTCHA parameters. Check the reCAPTCHA docs for a comprehensive list. All parameters are accepted except for the sitekey. Firebase Auth backend provisions a reCAPTCHA for each project and will configure this upon rendering. For an invisible reCAPTCHA, a size key must have the value 'invisible'.
             * @param container The corresponding Firebase app. If none is provided, the default Firebase App instance is used. A Firebase App instance must be initialized with an API key, otherwise an error will be thrown.
             */
            new(container: Element | string, parameters?: { theme?: "dark" | "light", type?: "audio" | "image", size?: "compact" | "normal", tabindex?: number, callback?: string, 'expired-callback'?: string }, app?: firebase.app.App): RecaptchaVerifier
            prototype: RecaptchaVerifier
        }
        var TwitterAuthProvider: {
            new(): TwitterAuthProvider
            prototype: TwitterAuthProvider

            PROVIDER_ID: string

            /**
             * @param token Twitter access token.
             * @param secret Twitter secret.
             */
            credential(token: string, secret: string): firebase.auth.AuthCredential
        }
        /**
         * A structure containing additional user information from a federated identity provider.
         */
        type AdditionalUserInfo = { providerId: string, profile?: Object, username?: string | undefined }
        /**
         * A structure containing a User, an AuthCredential, the operationType, and any additional user information that was returned from the identity provider. operationType could be 'signIn' for a sign-in operation, 'link' for a linking operation and 'reauthenticate' for a reauthentication operation.
         */
        type UserCredential = { user?: firebase.User, credential?: firebase.auth.AuthCredential, operationType?: string | undefined, additionalUserInfo?: firebase.auth.AdditionalUserInfo | undefined }
    }
    namespace database {
        namespace ServerValue {
            const TIMESTAMP: Object
        }
        interface Database {
            /**
             * The app associated with the Database service instance.
             */
            app: firebase.app.App

            /**
             * Disconnects from the server (all Database operations will be completed offline).
             * 
             * The client automatically maintains a persistent connection to the Database server, which will remain active indefinitely and reconnect when disconnected. However, the goOffline() and goOnline() methods may be used to control the client connection in cases where a persistent connection is undesirable.
             * 
             * While offline, the client will no longer receive data updates from the Database. However, all Database operations performed locally will continue to immediately fire events, allowing your application to continue behaving normally. Additionally, each operation performed locally will automatically be queued and retried upon reconnection to the Database server.
             * 
             * To reconnect to the Database and begin receiving remote events, see goOnline().
             */
            goOffline(): void
            /**
             * Reconnects to the server and synchronizes the offline Database state with the server state.
             * 
             * This method should be used after disabling the active connection with goOffline(). Once reconnected, the client will transmit the proper data and fire the appropriate events so that your client "catches up" automatically.
             */
            goOnline(): void
            /**
             * Returns a Reference representing the location in the Database corresponding to the provided path. If no path is provided, the Reference will point to the root of the Database.
             * @param path Optional path representing the location the returned Reference will point. If not provided, the returned Reference will point to the root of the Database.
             */
            ref(path?: string): firebase.database.Reference
            /**
             * Returns a Reference representing the location in the Database corresponding to the provided Firebase URL.
             * 
             * An exception is thrown if the URL is not a valid Firebase Database URL or it has a different domain than the current Database instance.
             * 
             * Note that all query parameters (orderBy, limitToLast, etc.) are ignored and are not applied to the returned Reference.
             * @param url The Firebase URL at which the returned Reference will point.
             */
            refFromURL(url: string): firebase.database.Reference
        }
        interface DataSnapshot {
            /**
             * The key (last part of the path) of the location of this DataSnapshot.
             * 
             * The last token in a Database location is considered its key. For example, "ada" is the key for the /users/ada/ node. Accessing the key on any DataSnapshot will return the key for the location that generated it. However, accessing the key on the root URL of a Database will return null.
             */
            key?: string
            /**
             * The Reference for the location that generated this DataSnapshot.
             */
            ref: firebase.database.Reference

            /**
             * Gets another DataSnapshot for the location at the specified relative path.
             * 
             * Passing a relative path to the child() method of a DataSnapshot returns another DataSnapshot for the location at the specified relative path. The relative path can either be a simple child name (for example, "ada") or a deeper, slash-separated path (for example, "ada/name/first"). If the child location has no data, an empty DataSnapshot (that is, a DataSnapshot whose value is null) is returned.
             * @param path A relative path to the location of child data.
             */
            child(path: string): firebase.database.DataSnapshot
            /**
             * Returns true if this DataSnapshot contains any data. It is slightly more efficient than using snapshot.val() !== null.
             */
            exists(): boolean
            /**
             * Exports the entire contents of the DataSnapshot as a JavaScript object.
             * 
             * The exportVal() method is similar to val(), except priority information is included (if available), making it suitable for backing up your data.
             */
            exportVal(): any
            /**
             * Enumerates the top-level children in the DataSnapshot.
             * 
             * Because of the way JavaScript objects work, the ordering of data in the JavaScript object returned by val() is not guaranteed to match the ordering on the server nor the ordering of child_added events. That is where forEach() comes in handy. It guarantees the children of a DataSnapshot will be iterated in their query order.
             * 
             * If no explicit orderBy*() method is used, results are returned ordered by key (unless priorities are used, in which case, results are returned by priority).
             * @param action A function that will be called for each child DataSnapshot. The callback can return true to cancel further enumeration.
             * @returns true if enumeration was canceled due to your callback returning true.
             */
            forEach(action: (childSnapshot: firebase.database.DataSnapshot) => void): boolean
            /**
             * Gets the priority value of the data in this DataSnapshot.
             * 
             * Applications need not use priority but can order collections by ordinary properties (see Sorting and filtering data).
             */
            getPriority(): string | number | null
            /**
             * Returns true if the specified child path has (non-null) data.
             * @param path A relative path to the location of a potential child.
             */
            hasChild(path: string): boolean
            /**
             * Returns whether or not the DataSnapshot has any non-null child properties.
             * 
             * You can use hasChildren() to determine if a DataSnapshot has any children. If it does, you can enumerate them using forEach(). If it doesn't, then either this snapshot contains a primitive value (which can be retrieved with val()) or it is empty (in which case, val() will return null).
             */
            hasChildren(): boolean
            /**
             * Returns the number of child properties of this DataSnapshot.
             */
            numChildren(): number
            /**
             * Returns a JSON-serializable representation of this object.
             */
            toJSON(): Object
            /**
             * Extracts a JavaScript value from a DataSnapshot.
             * 
             * Depending on the data in a DataSnapshot, the val() method may return a scalar type (string, number, or boolean), an array, or an object. It may also return null, indicating that the DataSnapshot is empty (contains no data).
             */
            val(): any
        }
        interface OnDisconnect {
            /**
             * Cancels all previously queued onDisconnect() set or update events for this location and all children.
             * 
             * If a write has been queued for this location via a set() or update() at a parent location, the write at this location will be canceled, though writes to sibling locations will still occur.
             * @param onComplete An optional callback function that will be called when synchronization to the server has completed. The callback will be passed a single parameter: null for success, or an Error object indicating a failure.
             */
            cancel(onComplete?: (error?: Error) => void): firebase.Promise<void>
            /**
             * Ensures the data at this location is deleted when the client is disconnected (due to closing the browser, navigating to a new page, or network issues).
             * @param onComplete An optional callback function that will be called when synchronization to the server has completed. The callback will be passed a single parameter: null for success, or an Error object indicating a failure.
             */
            remove(onComplete?: (error: Error | null) => void): firebase.Promise<void>
            /**
             * Ensures the data at this location is set to the specified value when the client is disconnected (due to closing the browser, navigating to a new page, or network issues).
             * 
             * set() is especially useful for implementing "presence" systems, where a value should be changed or cleared when a user disconnects so that they appear "offline" to other users. See Enabling Offline Capabilities in JavaScript for more information.
             * 
             * Note that onDisconnect operations are only triggered once. If you want an operation to occur each time a disconnect occurs, you'll need to re-establish the onDisconnect operations each time.
             * @param value The value to be written to this location on disconnect (can be an object, array, string, number, boolean, or null).
             * @param onComplete An optional callback function that will be called when synchronization to the server has completed. The callback will be passed a single parameter: null for success, or an Error object indicating a failure.
             */
            set(value: any, onComplete?: (error?: Error) => void): firebase.Promise<void>
            /**
             * Ensures the data at this location is set to the specified value and priority when the client is disconnected (due to closing the browser, navigating to a new page, or network issues).
             */
            setWithPriority(value: any, priority: number | string | null, onComplete?: (error?: Error) => void): firebase.Promise<void>
            /**
             * Writes multiple values at this location when the client is disconnected (due to closing the browser, navigating to a new page, or network issues).
             * 
             * The values argument contains multiple property-value pairs that will be written to the Database together. Each child property can either be a simple property (for example, "name") or a relative path (for example, "name/first") from the current location to the data to update.
             * 
             * As opposed to the set() method, update() can be use to selectively update only the referenced properties at the current location (instead of replacing all the child properties at the current location).
             * @param values Object containing multiple values.
             */
            update(values: Object, onComplete: (error: Error | null) => void): firebase.Promise<void>
        }
        interface QueryEventMap {
            "value"
            "child_added"
            "child_changed"
            "child_removed"
            "child_moved"
        }
        interface Query {
            /**
             * Returns a Reference to the Query's location.
             */
            ref: firebase.database.Reference

            /**
             * Creates a Query with the specified ending point.
             * 
             * Using startAt(), endAt(), and equalTo() allows you to choose arbitrary starting and ending points for your queries.
             * 
             * The ending point is inclusive, so children with exactly the specified value will be included in the query. The optional key argument can be used to further limit the range of the query. If it is specified, then children that have exactly the specified value must also have a key name less than or equal to the specified key.
             * @param value The value to end at. The argument type depends on which orderBy*() function was used in this query. Specify a value that matches the orderBy*() type. When used in combination with orderByKey(), the value must be a string.
             * @param key The child key to end at, among the children with the previously specified priority. This argument is only allowed if ordering by child, value, or priority.
             */
            endAt(value: number | string | boolean | null, key?: string): firebase.database.Query
            /**
             * Creates a Query that includes children that match the specified value.
             * 
             * Using startAt(), endAt(), and equalTo() allows you to choose arbitrary starting and ending points for your queries.
             * 
             * The ending point is inclusive, so children with exactly the specified value will be included in the query. The optional key argument can be used to further limit the range of the query. If it is specified, then children that have exactly the specified value must also have a key name less than or equal to the specified key.
             * @param value The value to end at. The argument type depends on which orderBy*() function was used in this query. Specify a value that matches the orderBy*() type. When used in combination with orderByKey(), the value must be a string.
             * @param key The child key to end at, among the children with the previously specified priority. This argument is only allowed if ordering by child, value, or priority.
             */
            equalTo(value: number | string | boolean | null, key: string): firebase.database.Query
            /**
             * Returns whether or not the current and provided queries represent the same location, have the same query parameters, and are from the same instance of firebase.app.App.
             * 
             * Two Reference objects are equivalent if they represent the same location and are from the same instance of firebase.app.App.
             * 
             * Two Query objects are equivalent if they represent the same location, have the same query parameters, and are from the same instance of firebase.app.App. Equivalent queries share the same sort order, limits, and starting and ending points.
             * @param other The query to compare against.
             */
            isEqual(other: firebase.database.Query): boolean
            /**
             * Generates a new Query limited to the first specific number of children.
             * 
             * The limitToFirst() method is used to set a maximum number of children to be synced for a given callback. If we set a limit of 100, we will initially only receive up to 100 child_added events. If we have fewer than 100 messages stored in our Database, a child_added event will fire for each message. However, if we have over 100 messages, we will only receive a child_added event for the first 100 ordered messages. As items change, we will receive child_removed events for each item that drops out of the active list so that the total number stays at 100.
             * @param limit The maximum number of nodes to include in this query.
             */
            limitToFirst(limit: number): firebase.database.Query
            /**
             * Generates a new Query object limited to the last specific number of children.
             * 
             * The limitToLast() method is used to set a maximum number of children to be synced for a given callback. If we set a limit of 100, we will initially only receive up to 100 child_added events. If we have fewer than 100 messages stored in our Database, a child_added event will fire for each message. However, if we have over 100 messages, we will only receive a child_added event for the last 100 ordered messages. As items change, we will receive child_removed events for each item that drops out of the active list so that the total number stays at 100.
             * @param limit The maximum number of nodes to include in this query.
             */
            limitToLast(limit: number): firebase.database.Query
            /**
             * Detaches a callback previously attached with on().
             * 
             * Detach a callback previously attached with on(). Note that if on() was called multiple times with the same eventType and callback, the callback will be called multiple times for each event, and off() must be called multiple times to remove the callback. Calling off() on a parent listener will not automatically remove listeners registered on child nodes, off() must also be called on any child listeners to remove the callback.
             * 
             * If a callback is not specified, all callbacks for the specified eventType will be removed. Similarly, if no eventType or callback is specified, all callbacks for the Reference will be removed.
             * @param eventType One of the following strings: "value", "child_added", "child_changed", "child_removed", or "child_moved."
             * @param callback The callback function that was passed to on().
             * @param context The context that was passed to on().
             */
            off<K extends keyof QueryEventMap>(eventType?: K, callback?: (snapshot: firebase.database.DataSnapshot, childKey?: string) => void, context?: Object): void
            /**
             * Detaches a callback previously attached with on().
             * 
             * Detach a callback previously attached with on(). Note that if on() was called multiple times with the same eventType and callback, the callback will be called multiple times for each event, and off() must be called multiple times to remove the callback. Calling off() on a parent listener will not automatically remove listeners registered on child nodes, off() must also be called on any child listeners to remove the callback.
             * 
             * If a callback is not specified, all callbacks for the specified eventType will be removed. Similarly, if no eventType or callback is specified, all callbacks for the Reference will be removed.
             * @param eventType One of the following strings: "value", "child_added", "child_changed", "child_removed", or "child_moved."
             * @param callback The callback function that was passed to on().
             * @param context The context that was passed to on().
             */
            off(eventType?: string, callback?: (snapshot: firebase.database.DataSnapshot, childKey?: string) => void, context?: Object): void
            /**
             * Listens for data changes at a particular location.
             * 
             * This is the primary way to read data from a Database. Your callback will be triggered for the initial data and again whenever the data changes. Use off( ) to stop receiving updates. See Retrieve Data on the Web for more details.
             * 
             * https://firebase.google.com/docs/reference/js/firebase.database.Query#on
             * @param eventType One of the following strings: "value", "child_added", "child_changed", "child_removed", or "child_moved."
             * @param callback A callback that fires when the specified event occurs. The callback will be passed a DataSnapshot. For ordering purposes, "child_added", "child_changed", and "child_moved" will also be passed a string containing the key of the previous child, by sort order, or null if it is the first child.
             * @param cancelCallbackOrContext An optional callback that will be notified if your event subscription is ever canceled because your client does not have permission to read this data (or it had permission but has now lost it). This callback will be passed an Error object indicating why the failure occurred.
             * @param context If provided, this object will be used as this when calling your callback(s).
             */
            on<K extends keyof QueryEventMap>(eventType: K, callback: (snapshot: firebase.database.DataSnapshot, childKey?: string) => void, cancelCallbackOrContext?: ((error: Error) => void) | Object, context?: Object): (snapshot: firebase.database.DataSnapshot, childKey?: string) => void
            /**
             * Listens for data changes at a particular location.
             * 
             * This is the primary way to read data from a Database. Your callback will be triggered for the initial data and again whenever the data changes. Use off( ) to stop receiving updates. See Retrieve Data on the Web for more details.
             * 
             * https://firebase.google.com/docs/reference/js/firebase.database.Query#on
             * @param eventType One of the following strings: "value", "child_added", "child_changed", "child_removed", or "child_moved."
             * @param callback A callback that fires when the specified event occurs. The callback will be passed a DataSnapshot. For ordering purposes, "child_added", "child_changed", and "child_moved" will also be passed a string containing the key of the previous child, by sort order, or null if it is the first child.
             * @param cancelCallbackOrContext An optional callback that will be notified if your event subscription is ever canceled because your client does not have permission to read this data (or it had permission but has now lost it). This callback will be passed an Error object indicating why the failure occurred.
             * @param context If provided, this object will be used as this when calling your callback(s).
             */
            on(eventType: string, callback: (snapshot: firebase.database.DataSnapshot, childKey?: string) => void, cancelCallbackOrContext?: ((error: Error) => void) | Object, context?: Object): (snapshot: firebase.database.DataSnapshot, childKey?: string) => void
            /**
             * Listens for exactly one event of the specified event type, and then stops listening.
             * 
             * This is equivalent to calling on(), and then calling off() inside the callback function. See on() for details on the event types.
             * @param eventType One of the following strings: "value", "child_added", "child_changed", "child_removed", or "child_moved."
             * @param callback A callback that fires when the specified event occurs. The callback will be passed a DataSnapshot. For ordering purposes, "child_added", "child_changed", and "child_moved" will also be passed a string containing the key of the previous child, by sort order, or null if it is the first child.
             * @param cancelCallbackOrContext An optional callback that will be notified if your event subscription is ever canceled because your client does not have permission to read this data (or it had permission but has now lost it). This callback will be passed an Error object indicating why the failure occurred.
             * @param context If provided, this object will be used as this when calling your callback(s).
             */
            once<K extends keyof QueryEventMap>(eventType: K, callback?: (snapshot: firebase.database.DataSnapshot, childKey?: string) => void, cancelCallbackOrContext?: ((error: Error) => void) | Object, context?: Object): firebase.Promise<any>
            /**
             * Listens for exactly one event of the specified event type, and then stops listening.
             * 
             * This is equivalent to calling on(), and then calling off() inside the callback function. See on() for details on the event types.
             * @param eventType One of the following strings: "value", "child_added", "child_changed", "child_removed", or "child_moved."
             * @param callback A callback that fires when the specified event occurs. The callback will be passed a DataSnapshot. For ordering purposes, "child_added", "child_changed", and "child_moved" will also be passed a string containing the key of the previous child, by sort order, or null if it is the first child.
             * @param cancelCallbackOrContext An optional callback that will be notified if your event subscription is ever canceled because your client does not have permission to read this data (or it had permission but has now lost it). This callback will be passed an Error object indicating why the failure occurred.
             * @param context If provided, this object will be used as this when calling your callback(s).
             */
            once(eventType: string, callback?: (snapshot: firebase.database.DataSnapshot, childKey?: string) => void, cancelCallbackOrContext?: ((error: Error) => void) | Object, context?: Object): firebase.Promise<any>
            /**
             * Generates a new Query object ordered by the specified child key.
             * 
             * Queries can only order by one key at a time. Calling orderByChild() multiple times on the same query is an error.
             * 
             * Firebase queries allow you to order your data by any child key on the fly. However, if you know in advance what your indexes will be, you can define them via the .indexOn rule in your Security Rules for better performance. See the .indexOn rule for more information.
             */
            orderByChild(path: string): firebase.database.Query
            /**
             * Generates a new Query object ordered by key.
             * 
             * Sorts the results of a query by their (ascending) key values.
             */
            orderByKey(): firebase.database.Query
            /**
             * Generates a new Query object ordered by priority.
             * 
             * Applications need not use priority but can order collections by ordinary properties (see Sort data for alternatives to priority.
             */
            orderByPriority(): firebase.database.Query
            /**
             * Generates a new Query object ordered by value.
             * 
             * If the children of a query are all scalar values (string, number, or boolean), you can order the results by their (ascending) values.
             */
            orderByValue(): firebase.database.Query
            /**
             * Creates a Query with the specified starting point.
             * 
             * Using startAt(), endAt(), and equalTo() allows you to choose arbitrary starting and ending points for your queries.
             * 
             * The starting point is inclusive, so children with exactly the specified value will be included in the query. The optional key argument can be used to further limit the range of the query. If it is specified, then children that have exactly the specified value must also have a key name greater than or equal to the specified key.
             * @param value The value to start at. The argument type depends on which orderBy*() function was used in this query. Specify a value that matches the orderBy*() type. When used in combination with orderByKey(), the value must be a string.
             * @param key The child key to start at. This argument is only allowed if ordering by child, value, or priority.
             */
            startAt(value: number | string | boolean | null, key?: string): firebase.database.Query
            /**
             * Returns a JSON-serializable representation of this object.
             */
            toJSON(): Object
            /**
             * Gets the absolute URL for this location.
             * 
             * The toString() method returns a URL that is ready to be put into a browser, curl command, or a firebase.database().refFromURL() call. Since all of those expect the URL to be url-encoded, toString() returns an encoded URL.
             * 
             * Append '.json' to the returned URL when typed into a browser to download JSON-formatted data. If the location is secured (that is, not publicly readable), you will get a permission-denied error.
             * @returns The absolute URL for this location.
             */
            toString(): string
        }
        interface Reference extends Query {
            /**
             * The last part of the Reference's path.
             * 
             * For example, "ada" is the key for https://<DATABASE_NAME>.firebaseio.com/users/ada.
             * 
             * The key of a root Reference is null.
             */
            key: string | null
            /**
             * The parent location of a Reference.
             * 
             * The parent of a root Reference is null.
             */
            parent?: firebase.database.Reference
            /**
             * The root Reference of the Database.
             */
            root: firebase.database.Reference

            /**
             * Gets a Reference for the location at the specified relative path.
             * 
             * The relative path can either be a simple child name (for example, "ada") or a deeper slash-separated path (for example, "ada/name/first").
             */
            child(path: string): firebase.database.Reference
            /**
             * Returns an OnDisconnect object - see Enabling Offline Capabilities in JavaScript for more information on how to use it.
             */
            onDisconnect(): firebase.database.OnDisconnect
            /**
             * Generates a new child location using a unique key and returns its Reference.
             * 
             * This is the most common pattern for adding data to a collection of items.
             * 
             * If you provide a value to push(), the value will be written to the generated location. If you don't pass a value, nothing will be written to the Database and the child will remain empty (but you can use the Reference elsewhere).
             * 
             * The unique key generated by push() are ordered by the current time, so the resulting list of items will be chronologically sorted. The keys are also designed to be unguessable (they contain 72 random bits of entropy).
             * @param value Optional value to be written at the generated location.
             * @param onComplete Callback called when write to server is complete.
             */
            push(value?: any, onComplete?: (error?: Error) => void): firebase.database.ThenableReference
            /**
             * Removes the data at this Database location.
             * 
             * Any data at child locations will also be deleted.
             * 
             * The effect of the remove will be visible immediately and the corresponding event 'value' will be triggered. Synchronization of the remove to the Firebase servers will also be started, and the returned Promise will resolve when complete. If provided, the onComplete callback will be called asynchronously after synchronization has finished.
             */
            remove(onComplete?: (error?: Error) => void): firebase.Promise<void>
            /**
             * Writes data to this Database location.
             * 
             * This will overwrite any data at this location and all child locations.
             * 
             * The effect of the write will be visible immediately, and the corresponding events ("value", "child_added", etc.) will be triggered. Synchronization of the data to the Firebase servers will also be started, and the returned Promise will resolve when complete. If provided, the onComplete callback will be called asynchronously after synchronization has finished.
             * 
             * Passing null for the new value is equivalent to calling remove(); namely, all data at this location and all child locations will be deleted.
             * 
             * set() will remove any priority stored at this location, so if priority is meant to be preserved, you need to use setWithPriority() instead.
             * 
             * Note that modifying data with set() will cancel any pending transactions at that location, so extreme care should be taken if mixing set() and transaction() to modify the same data.
             * 
             * A single set() will generate a single "value" event at the location where the set() was performed.
             * @param value Optional value to be written at the generated location.
             * @param onComplete Callback called when write to server is complete.
             */
            set(value: any, onComplete?: (error?: Error) => void): firebase.Promise<void>
            /**
             * Sets a priority for the data at this Database location.
             * 
             * Applications need not use priority but can order collections by ordinary properties (see Sorting and filtering data).
             */
            setPriority(priority: number | string | null): firebase.Promise<void>
            /**
             * Writes data the Database location. Like set() but also specifies the priority for that data.
             * 
             * Applications need not use priority but can order collections by ordinary properties (see Sorting and filtering data).
             */
            setWithPriority(newVal: any, priority: number | string | null, onComplete?: (error?: Error) => void): firebase.Promise<void>
            /**
             * Atomically modifies the data at this location.
             * 
             * Atomically modify the data at this location. Unlike a normal set(), which just overwrites the data regardless of its previous value, transaction() is used to modify the existing value to a new value, ensuring there are no conflicts with other clients writing to the same location at the same time.
             * 
             * To accomplish this, you pass transaction() an update function which is used to transform the current value into a new value. If another client writes to the location before your new value is successfully written, your update function will be called again with the new current value, and the write will be retried. This will happen repeatedly until your write succeeds without conflict or you abort the transaction by not returning a value from your update function.
             * 
             * Note: Modifying data with set() will cancel any pending transactions at that location, so extreme care should be taken if mixing set() and transaction() to update the same data.
             * 
             * Note: When using transactions with Security and Firebase Rules in place, be aware that a client needs .read access in addition to .write access in order to perform a transaction. This is because the client-side nature of transactions requires the client to read the data in order to transactionally update it.
             * @param transactionUpdate A developer-supplied function which will be passed the current data stored at this location (as a JavaScript object). The function should return the new value it would like written (as a JavaScript object). If undefined is returned (i.e. you return with no arguments) the transaction will be aborted and the data at this location will not be modified.
             * @param onComplete A callback function that will be called when the transaction completes. The callback is passed three arguments: a possibly-null Error, a boolean indicating whether the transaction was committed, and a DataSnapshot indicating the final result. If the transaction failed abnormally, the first argument will be an Error object indicating the failure cause. If the transaction finished normally, but no data was committed because no data was returned from transactionUpdate, then second argument will be false. If the transaction completed and committed data to Firebase, the second argument will be true. Regardless, the third argument will be a DataSnapshot containing the resulting data in this location.
             * @param applyLocally By default, events are raised each time the transaction update function runs. So if it is run multiple times, you may see intermediate states. You can set this to false to suppress these intermediate states and instead wait until the transaction has completed before events are raised.
             */
            transaction(transactionUpdate: (update: any) => void, onComplete?: (error: Error | null) => void, applyLocally?: boolean): firebase.Promise<{ comitted: boolean, snapshot?: firebase.database.DataSnapshot }>
            /**
             * Writes multiple values to the Database at once.
             * 
             * The values argument contains multiple property-value pairs that will be written to the Database together. Each child property can either be a simple property (for example, "name") or a relative path (for example, "name/first") from the current location to the data to update.
             * 
             * As opposed to the set() method, update() can be use to selectively update only the referenced properties at the current location (instead of replacing all the child properties at the current location).
             * 
             * The effect of the write will be visible immediately, and the corresponding events ('value', 'child_added', etc.) will be triggered. Synchronization of the data to the Firebase servers will also be started, and the returned Promise will resolve when complete. If provided, the onComplete callback will be called asynchronously after synchronization has finished.
             * 
             * A single update() will generate a single "value" event at the location where the update() was performed, regardless of how many children were modified.
             * 
             * Note that modifying data with update() will cancel any pending transactions at that location, so extreme care should be taken if mixing update() and transaction() to modify the same data.
             * 
             * Passing null to update() will remove the data at this location.
             * @param values Object containing multiple values.
             * @param onComplete Callback called when write to server is complete.
             */
            update(values: Object, onComplete?: (error?: Error) => void): firebase.Promise<void>
        }
        interface ThenableReference extends /*firebase.Promise, */Reference {}
        /**
         * Logs debugging information to the console.
         * @param logger Enables logging if true; disables logging if false. You can also provide a custom logger function to control how things get logged.
         * @param persistent Remembers the logging state between page refreshes if true.
         */
        function enableLogging(logger?: boolean | ((value: string) => void), persistent?: boolean): void
    }
    namespace messaging {
        interface Messaging {
            /**
             * To forceably stop a registration token from being used, delete it by calling this method.
             * @param token The token to delete.
             */
            deleteToken(token: string): firebase.Promise<void>
            /**
             * After calling requestPermission() you can call this method to get an FCM registration token that can be used to send push messages to this user.
             */
            getToken(): firebase.Promise<string>
            /**
             * When a push message is received and the user is currently on a page for your origin, the message is passed to the page and an onMessage() event is dispatched with the payload of the push message.
             * 
             * NOTE: These events are dispatched when you have called setBackgroundMessageHandler() in your service worker.
             * @param nextOrObserver This function, or observer object with next defined, is called when a message is received and the user is currently viewing your page.
             * @returns To stop listening for messages execute this returned function.
             */
            onMessage(nextOrObserver: ((message: Object) => void) | Object): Function
            /**
             * You should listen for token refreshes so your web app knows when FCM has invalidated your existing token and you need to call getToken() to get a new token.
             * @param nextOrObserver This function, or observer object with next defined, is called when a token refresh has occurred.
             * @returns To stop listening for messages execute this returned function.
             */
            onTokenRefresh(nextOrObserver: ((token: Object) => void) | Object): Function
            /**
             * Notification permissions are required to send a user push messages. Calling this method displays the permission dialog to the user and resolves if the permission is granted.
             */
            requestPermission(): firebase.Promise<void>
            /**
             * FCM directs push messages to your web page's onMessage() callback if the user currently has it open. Otherwise, it calls your callback passed into setBackgroundMessageHandler().
             * 
             * Your callback should return a promise that, once resolved, has shown a notification.
             * @param callback The function to handle the push message.
             */
            setBackgroundMessageHandler(callback: (message: Object) => void): void
            /**
             * To use your own service worker for receiving push messages, you can pass in your service worker registration in this method.
             * @param registration The service worker registration you wish to use for push messaging.
             */
            useServiceWorker(registration: ServiceWorkerRegistration): void
        }
    }
    namespace storage {
        interface FullMetadata extends UploadMetadata {
            /**
             * The bucket this object is contained in.
             */
            bucket: string
            /**
             * An array of long-lived download URLs. Always contains at least one URL.
             */
            downloadURLs: string[]
            /**
             * The full path of this object.
             */
            fullPath: string
            /**
             * The object's generation.
             * https://cloud.google.com/storage/docs/generations-preconditions
             */
            generation: string
            /**
             * The object's metageneration.
             * https://cloud.google.com/storage/docs/generations-preconditions
             */
            metageneration: string
            /**
             * The short name of this object, which is the last component of the full path. For example, if fullPath is 'full/path/image.png', name is 'image.png'.
             */
            name: string
            /**
             * The size of this object, in bytes.
             */
            size: number
            /**
             * A date string representing when this object was created.
             */
            timeCreated: string
            /**
             * A date string representing when this object was last updated.
             */
            updated: string
        }
        interface Reference {
            /**
             * The name of the bucket containing this reference's object.
             */
            bucket: string
            /**
             * The full path of this object.
             */
            fullPath: string
            /**
             * The short name of this object, which is the last component of the full path. For example, if fullPath is 'full/path/image.png', name is 'image.png'.
             */
            name: string
            /**
             * A reference pointing to the parent location of this reference, or null if this reference is the root.
             */
            parent?: firebase.storage.Reference
            /**
             * A reference to the root of this reference's bucket.
             */
            root: firebase.storage.Reference
            /**
             * The storage service associated with this reference.
             */
            storage: firebase.storage.Storage

            /**
             * Returns a reference to a relative path from this reference.
             * @param path The relative path from this reference. Leading, trailing, and consecutive slashes are removed.
             */
            child(path: string): firebase.storage.Reference
            /**
             * Deletes the object at this reference's location.
             * @returns A Promise that resolves if the deletion succeeded and rejects if it failed, including if the object didn't exist.
             */
            delete(): firebase.Promise<string>
            /**
             * Fetches a long lived download URL for this object.
             * @returns A Promise that resolves if the deletion succeeded and rejects if it failed, including if the object didn't exist.
             */
            getDownloadURL(): firebase.Promise<string>
            /**
             * Fetches metadata for the object at this location, if one exists.
             * @returns A Promise that resolves if the deletion succeeded and rejects if it failed, including if the object didn't exist.
             */
            getMetadata(): firebase.Promise<firebase.storage.FullMetadata>
            /**
             * Uploads data to this reference's location.
             * @param data The data to upload.
             * @param metadata Metadata for the newly uploaded object.
             */
            put(data: Blob | Uint8Array | ArrayBuffer, metadata?: firebase.storage.UploadMetadata): firebase.storage.UploadTask
            /**
             * Uploads string data to this reference's location.
             * @param data The string to upload.
             * @param format The format of the string to upload.
             * @param metadata Metadata for the newly uploaded object.
             * @throws If the format is not an allowed format, or if the given string doesn't conform to the specified format.
             */
            putString(data: string, format?: firebase.storage.StringFormat, metadata?: firebase.storage.UploadMetadata): firebase.storage.UploadTask
            /**
             * Returns a gs:// URL for this object in the form gs://<bucket>/<path>/<to>/<object>
             */
            toString(): string
            /**
             * Updates the metadata for the object at this location, if one exists.
             * @param metadata The new metadata. Setting a property to 'null' removes it on the server, while leaving a property as 'undefined' has no effect.
             * @returns A Promise that resolves if the deletion succeeded and rejects if it failed, including if the object didn't exist.
             */
            updateMetadata(metadata: firebase.storage.SettableMetadata): firebase.Promise<firebase.storage.FullMetadata>
        }
        interface SettableMetadata {
            /**
             * Served as the 'Cache-Control' header on object download.
             */
            cacheControl?: string | undefined
            /**
             * Served as the 'Content-Disposition' header on object download.
             */
            contentDisposition?: string | undefined
            /**
             * Served as the 'Content-Encoding' header on object download.
             */
            contentEncoding?: string | undefined
            /**
             * Served as the 'Content-Language' header on object download.
             */
            contentLanguage?: string | undefined
            /**
             * Served as the 'Content-Type' header on object download.
             */
            contentType?: string | undefined
            /**
             * Additional user-defined custom metadata.
             */
            customMetadata?: Object | undefined
        }
        interface Storage {
            /**
             * The app associated with the Storage service instance.
             */
            app: firebase.app.App
            /**
             * The maximum time to retry operations other than uploads or downloads in milliseconds.
             */
            maxOperationRetryTime: number
            /**
             * The maximum time to retry uploads in milliseconds.
             */
            maxUploadRetryTime: number

            /**
             * Returns a reference for the given path in the default bucket.
             * @param path A relative path to initialize the reference with, for example path/to/image.jpg. If not passed, the returned reference points to the bucket root.
             */
            ref(path?: string): firebase.storage.Reference
            /**
             * Returns a reference for the given absolute URL.
             * @param url A URL in the form: 1) a gs:// URL, for example gs://bucket/files/image.png. 2) a download URL taken from object metadata. 
             */
            refFromURL(url: string): firebase.storage.Reference
            /**
             * @param time The new maximum operation retry time in milliseconds.
             */
            setMaxOperationRetryTime(time: number): void
            /**
             * @param time The new maximum upload retry time in milliseconds.
             */
            setMaxUploadRetryTime(time: number): void
        }
        interface UploadMetadata extends SettableMetadata {
            /**
             * A Base64-encoded MD5 hash of the object being uploaded.
             */
            md5Hash?: string | undefined
        }
        interface UploadTask {
            /**
             * A snapshot of the current task state.
             */
            snapshot: firebase.storage.UploadTaskSnapshot

            /**
             * Cancels a running task. Has no effect on a complete or failed task.
             */
            cancel(): boolean
            /**
             * Equivalent to calling then(null, onRejected).
             */
            catch(onRejected: (error: Error) => void): firebase.Promise<void>
            /**
             * Listens for events on this task.
             * 
             * Events have three callback functions (referred to as next, error, and complete).
             * 
             * If only the event is passed, a function that can be used to register the callbacks is returned. Otherwise, the callbacks are passed after the event.
             * 
             * Callbacks can be passed either as three separate arguments or as the next, error, and complete properties of an object. Any of the three callbacks is optional, as long as at least one is specified. In addition, when you add your callbacks, you get a function back. You can call this function to unregister the associated callbacks.
             * @param event The event to listen for.
             * @param nextOrObserver The next function, which gets called for each item in the event stream, or an observer object with some or all of these three properties (next, error, complete).
             * @param error A function that gets called with an Error if the event stream ends due to an error.
             * @param complete A function that gets called if the event stream ends normally.
             */
            on(event: firebase.storage.TaskEvent, nextOrObserver?: ((snapshot: Object) => void) | Object, error?: ((error: Error) => void), complete?: Object): (snapshot: firebase.database.DataSnapshot, childKey?: string) => void
            /**
             * Pauses a running task. Has no effect on a paused or failed task.
             */
            pause(): boolean
            /**
             * Resumes a paused task. Has no effect on a running or failed task.
             */
            resume(): boolean
            /**
             * This object behaves like a Promise, and resolves with its snapshot data when the upload completes.
             */
            then(onFulfilled?: (value: firebase.storage.UploadTaskSnapshot) => any, onRejected?: (value: Error) => any): firebase.Promise<void>
        }
        interface UploadTaskSnapshot {
            /**
             * The number of bytes that have been successfully uploaded so far.
             */
            bytesTransferred: number
            /**
             * After the upload completes, contains a long-lived download URL for the object. Also accessible in metadata.
             */
            downloadURL?: string
            /**
             * Before the upload completes, contains the metadata sent to the server. After the upload completes, contains the metadata sent back from the server.
             */
            metadata: firebase.storage.FullMetadata
            /**
             * The reference that spawned this snapshot's upload task.
             */
            ref: firebase.storage.Reference
            /**
             * The current state of the task.
             */
            state: firebase.storage.TaskState
            /**
             * The task of which this is a snapshot.
             */
            task: firebase.storage.UploadTask
            /**
             * The total number of bytes to be uploaded.
             */
            totalBytes: number
        }
        enum StringFormat {
            /**
             * Indicates the string should be interpreted "raw", that is, as normal text. The string will be interpreted as UTF-16, then uploaded as a UTF-8 byte sequence. Example: The string 'Hello! \ud83d\ude0a' becomes the byte sequence 48 65 6c 6c 6f 21 20 f0 9f 98 8a
             */
            RAW,
            /**
             * Indicates the string should be interpreted as base64-encoded data. Padding characters (trailing '='s) are optional. Example: The string 'rWmO++E6t7/rlw==' becomes the byte sequence ad 69 8e fb e1 3a b7 bf eb 97
             */
            BASE64,
            /**
             * Indicates the string should be interpreted as base64url-encoded data. Padding characters (trailing '='s) are optional. Example: The string 'rWmO--E6t7_rlw==' becomes the byte sequence ad 69 8e fb e1 3a b7 bf eb 97
             */
            BASE64URL,
            /**
             * Indicates the string is a data URL, such as one obtained from canvas.toDataURL(). Example: the string 'data:application/octet-stream;base64,aaaa' becomes the byte sequence 69 a6 9a (the content-type "application/octet-stream" is also applied, but can be overridden in the metadata object).
             */
            DATA_URL
        }
        enum TaskEvent {
            /**
             * For this event,
             * The next function is triggered on progress updates and when the task is paused/resumed with a firebase.storage.UploadTaskSnapshot as the first argument.
             * The error function is triggered if the upload is canceled or fails for another reason.
             * The complete function is triggered if the upload completes successfully.
             */
            STATE_CHANGED
        }
        enum TaskState {
            /**
             * Indicates that the task is still running and making progress.
             */
            RUNNING,
            /**
             * Indicates that the task is paused.
             */
            PAUSED,
            /**
             * Indicates that the task completed successfully.
             */
            SUCCESS,
            /**
             * Indicates that the task was canceled.
             */
            CANCELED,
            /**
             * Indicates that the task failed for a reason other than being canceled.
             */
            ERROR
        }
    }

    var apps: firebase.app.App
    var SDK_VERSION: string

    /**
     * Creates and initializes a Firebase app instance.
     *
     * See Add Firebase to your app and Initialize multiple apps for detailed documentation.
     * @param config Options to configure the app's services.
     * @param name Optional name of the app to initialize. If no name is provided, the default is "[DEFAULT]".
     */
    function initializeApp(config: {
                    apiKey?: string
                    authDomain?: string
                    databaseURL?: string
                    projectId?: string
                    storageBucket?: string
                    messagingSenderId?: string
                }, name?: string): firebase.app.App
    /**
     * Retrieves a Firebase app instance.
     * 
     * When called with no arguments, the default app is returned. When an app name is provided, the app corresponding to that name is returned.
     * 
     * An exception is thrown if the app being retrieved has not yet been initialized.
     * @param name Optional name of the app to return. If no name is provided, the default is "[DEFAULT]".
     */
    function app(name?: string): firebase.app.App
    /**
     * Gets the Auth service for the default app or a given app.
     *
     * firebase.auth() can be called with no arguments to access the default app's Auth service or as firebase.auth(app) to access the Auth service associated with a specific app.
     * @param app 
     */
    function auth(app?: firebase.app.App): firebase.auth.Auth
    /**
     * Gets the Database service for the default app or a given app.
     * 
     * firebase.database() can be called with no arguments to access the default app's Database service or as firebase.database(app) to access the Database service associated with a specific app.
     * 
     * firebase.database is also a namespace that can be used to access global constants and methods associated with the Database service.
     * @param app Optional app whose Database service to return. If not provided, the default Database service will be returned.
     */
    function database(app?: firebase.app.App): firebase.database.Database
    /**
     * Gets the Messaging service for the default app or a given app.
     * 
     * firebase.messaging() can be called with no arguments to access the default app's Messaging service or as firebase.messaging(app) to access the Messaging service associated with a specific app.
     * 
     * Calling firebase.messaging() in a service worker results in Firebase generating notifications if the push message payload has a notification parameter.
     * @param app The app to create a Messaging service for. If not passed, uses the default app.
     */
    function messaging(app?: firebase.app.App): firebase.database.Database
}