/*type DOMString = string
type RequestMode = 'same-origin' | 'no-cors' | 'cors' | 'navigate'
type RequestCredentials = 'omit' | 'same-origin' | 'include'
type RequestRedirect = 'follow' | 'error' | 'manual'
type RequestCache = 'default' | 'no-store' | 'reload' | 'no-cahe' | 'force-cahe' | 'onlt-if-cahed'
declare interface FetchRequest extends Body {
    method: ByteString
    url: USVString
    headers: Headers
    referrer: DOMString
    referrerPolicy: DOMString
    mode: RequestMode
    credentials: RequestCredentials
    redirect: RequestRedirect
    integrity
    cache: RequestCache
}
declare interface FetchResponse extends Body {
    clone(): Response
    error(): Response
    redirect(): Response
}
declare function fetch(request: string | FetchRequest): Promise<FetchResponse>
*/