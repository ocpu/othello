class FancyTextInput extends HTMLElement {
    constructor() {
        super()
        this._label = ''
        this._value = ''
        this._name = 'text'
        this.attachShadow({ mode: 'open' })
    }

    set label(value) {
        return this._label = value
    }

    get label() {
        return this._label
    }

    set name(value) {
        return this._label = value
    }

    get name() {
        return this._label
    }

    set value(value) {
        this._value = value
        if (this.shadowRoot && this.shadowRoot.childNodes.length > 0) {
            return this.shadowRoot.querySelector('input').value = value
        }
        return value
    }

    get value() {
        return this._value
    }

    getAttr(name) {
        const attr = this.attributes.getNamedItem(name)
        if (attr) return attr.value
        return void 0
    }

    connectedCallback() {
        this._label = this.getAttr('label') || this._label
        this._value = this.getAttr('value') || this._value
        this._name = this.getAttr('name') || this._name
        if (this.shadowRoot.childNodes.length === 0) {
            const style = document.createElement('style')
            const input = document.createElement('input')
            const label = document.createElement('label')
            const container = document.createElement('div')
            container.appendChild(input)
            container.appendChild(label)
            this.shadowRoot.appendChild(style)
            this.shadowRoot.appendChild(container)
            input.id = 'text'
            input.type = 'text'
            input.name = this._name
            input.value = this._value
            label.for = 'text'
            label.appendChild(document.createTextNode(this._label))
            style.appendChild(document.createTextNode(`
div > input[type="text"] {
    display: inline-block;
    border: 0;
    border-bottom: 2px solid lightgray;
    font-size: 14px;
    outline: none;
    padding: 20px 0 5px;
    transition: border-bottom-color .2s ease;
}

div > input[type="text"]:focus {
    border-bottom-color: #000;
}

div > label {
    position: absolute;
    top: 24px;
    left: 0;
    right: 0;
    color: lightgray;
    cursor: text;
    transition: transform .2s ease, font-size .2s ease, color .2s ease;
}

div > input:focus + label,
div > input.has-text + label {
    font-size: 12px;
    transform: translateY(-20px);
    color: #000;
}

div {
    position: relative;
    display: inline-block;
    margin: 5px;
}
`))
        }
    }

    attributeChangedCallback(attrName, oldVal, newVal) {
        this[attrName] = newVal
    }

    static get observedAttributes() {
        return ['label', 'value']
    }
}

customElements.define('fancy-input', FancyTextInput)