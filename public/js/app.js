// Initialize Firebase
firebase.initializeApp({
    apiKey: "AIzaSyCvQsCk3lzrl10mA-Eok-zzHClQWy4rEkY",
    authDomain: "my-awesome-othello-project.firebaseapp.com",
    databaseURL: "https://my-awesome-othello-project.firebaseio.com",
    projectId: "my-awesome-othello-project",
    storageBucket: "my-awesome-othello-project.appspot.com",
    messagingSenderId: "989403460699",
})

WebSocket.prototype.on = WebSocket.prototype.addEventListener
WebSocket.prototype.off = WebSocket.prototype.removeEventListener
WebSocket.prototype.once = function (event, cb) {
    const self = this
    return new Promise((resolve, reject) => {
        this.addEventListener('message', function oncefn(evt) {
            const { e, d } = JSON.parse(evt.data)
            if (e === event) {
                if (typeof cb === 'function') {
                    try {
                        resolve(cb(d))
                    } catch(e) {
                        reject(e)
                    }
                } else {
                    try {
                        resolve(d)
                    } catch(e) {
                        reject(e)
                    }
                }
                self.removeEventListener('message', oncefn)
            }
        })
    })
}
WebSocket.prototype.sendCode = function (op, data) {
    this.send(JSON.stringify({ op, d: data }))
}

console.log(location)

const socket = new WebSocket(`${location.protocol.length === 5 ? 'ws' : 'wss'}://${location.host}/`)

const createVue = (element, setup) => new Vue(Object.assign({ el: element }, setup))

const app = createVue('#app', {
    data: {
        gameCode: "",
        hasGame: "",
        turn: 0,
        elements: [],
        spectating: false,
        points: {
            light: 2,
            dark: 2
        }
    },
    methods: {
        async create() {
            const res = await fetch('/new')
            this.gameCode = res.text()
            this.join()
        },
        join(e) {
            socket.addEventListener('message', ({ dat }) => {
                const { op, d: data } = JSON.parse(dat)
                switch (op) {
                    case 0:
                        break
                    case 1:
                        
                        break
                }
            })
            firebase.database().ref(`board/${this.gameCode}`).on('value', snapshot => {
                const value = snapshot.val()
                if (value === null)
                    return 
                console.log(value)
                this.elements = value.elements.split(',').map(state => ({ state }))
                this.points.light = value.points.light
                this.points.dark = value.points.dark
                if (!this.hasGame) setTimeout(() => document.querySelector('#board').classList.toggle('active', true), 1000)
                this.hasGame = true
            })
        },
        spectate(e) {
            this.spectating = true
            this.join()
        },
        leave(e) {
            // this.gameCode = ''
            this.hasGame = false
        },
        async place(e) {
            const tile = findTile(e)
            const idx = Array.from(tile.parentElement.children).indexOf(tile)
            socket.sendCode('PLACE', {
                code: this.gameCode,
                x: idx % 8 + 1,
                y: Math.floor(idx / 8 + 1),
            })
        }
    },
    computed: {
        whosTurn() {
            return this.turn % 2 ? 'black' : 'white'
        }
    }
})

function findTile(event) {
    const depth = arguments[1] | 0
    if (event.path[depth].dataset.tile !== void 0)
        return event.path[depth]
    return findTile(event, depth + 1)
}

socket.addEventListener('message', ({ data: message }) => {
    const {e: event, d: data } = JSON.parse(message)
    console.log({event, data})
    if (event === 'VALIDATED_PLACED_POSITION') {
    }
})