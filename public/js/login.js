new Vue({
    el: '[app]',
    data: {
        email: '',
        code: '',
        pass: '',
        eError: '',
        step: 0,
        login: true
    },
    methods: {
        changeLoginState(e) {
            e.preventDefault()
            this.login = !this.login
        },
        reset(e) {
            if (e.target.classList.contains('error')) e.target.classList.remove('error')
            e.target.classList.toggle('typing', !!e.target.value)
        },
        validateEmail(e) {
            e.preventDefault()
            if (!/.+/.test(this.email))
                return this.eError = new Error('Email is required')
            if (!this.email.includes('@'))
                return this.eError = new Error("Email should include '@'")
            if (!/[^@]+@.+/.test(this.email))
                return this.eError = new Error('Email is incomplete')
            firebase.auth().fetchProvidersForEmail(this.email).then(providers => {
                if (providers.length > 0)
                    this.eError = new Error('Email is already in use')
                // Next step: verify email
                console.log('Step Complete!')
            })
        },
        section(e) {
            e.preventDefault()
            firebase.auth().l()
        }
    }
})


