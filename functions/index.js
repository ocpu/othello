const functions = require('firebase-functions')

const admin = require('firebase-admin')
admin.initializeApp(functions.config().firebase)

const db = admin.database()

// Start writing Firebase Functions
// https://firebase.google.com/functions/write-firebase-functions

function acceptance(res) {
    res.setHeader("Access-Control-Allow-Origin", "*")
    res.setHeader("Access-Control-Allow-Credentials", "true")
    res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT")
    res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers")
}

exports.newSignup = functions.https.onRequest((req, res) => {
    
})

exports.newGame = functions.https.onRequest((req, res) => {
    const ref = db.ref('board').push({
        elements: new Array(8*8).fill('none', 0, 8*8).map((_, i) => i === 27 || i === 36 ? 'white' : i === 28 || i === 35 ? 'black' : _).join(','),
        history: "",
        points: {
            light: 2,
            dark: 2
        },
        turn: 0
    })
    acceptance(res)
    res.setHeader('Content-Type', 'text/plain;charset=utf-8')
    res.setHeader('Content-Length', ref.key.length)
    res.status(200)
    res.send(ref.key)
})

exports.checkPlacementValidity = functions.https.onRequest((req, res) => {
    if (req.method === 'GET') checkPlacementValidity(req.query).then(() => {
        acceptance(res)
        res.status(200)
        res.send()
    }).catch(err => {
        const message = JSON.stringify({ status: err.code, message: err.message })
        acceptance(res)
        res.setHeader('Content-Type', 'application/json;charset=utf-8')
        res.setHeader('Content-Length', message.length)
        res.status(err.code)
        res.send(message)
    })
})


exports.setPiece = functions.https.onRequest((req, res) => {
    if (req.method === 'GET') checkPlacementValidity(req.query).then(({ ref, items, turn, piecesAround, history }) => {
        loop:for (let { i: direction } of piecesAround) {
            const [cx, cy] = [req.query.x, req.query.y]
            do {
                switch(direction) {
                    case 0:case 6:case 7:cx--;break
                    case 2:case 3:case 4:cx++;break
                }
                switch(direction) {
                    case 0:case 1:case 2:cy--;break
                    case 4:case 5:case 6:cy++;break
                }
                if (items[indexFromPos(cx, cy)] === nextTurnChanges[turn % 2])
                    items[indexFromPos(cx, cy)] = nextTurnChanges[turn + 1 % 2]
                else continue loop
            } while (true)
        }
        const points = { dark: 0, light: 0 }
        for (let color of items)
            if (color === 'black') points.dark++
            else if (color === 'white') points.light++
        
        ref.set({
            elements: items.join(','),
            history: `${history}${turn === 0 ? '' : ','}(${x};${y})`,
            points,
            turn: turn + 1
        })
        acceptance(res)
        res.status(200)
        res.send()
    }).catch(err => {
        console.log(err)
        const message = JSON.stringify({ status: err.code, message: err.message })
        acceptance(res)
        res.setHeader('Content-Type', 'application/json;charset=utf-8')
        res.setHeader('Content-Length', message.length)
        res.status(err.code)
        res.send(message)
    })
            
})
