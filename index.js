const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const firebase = require('firebase-admin')
const WebSocketServer = require('ws').Server

firebase.initializeApp({
    credential: firebase.credential.cert(require('./firebase-othello')),
    databaseURL: "https://my-awesome-othello-project.firebaseio.com",
})

const app = express()

app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, 'views'))

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())

app.get('/new', (req, res) => {
    const { key } = firebase.database().ref('board').push({
        elements: new Array(8*8).fill('none', 0, 8*8).map((_, i) => i === 27 || i === 36 ? 'white' : i === 28 || i === 35 ? 'black' : _).join(','),
        history: "",
        points: {
            light: 2,
            dark: 2
        },
        users: '',
        turn: 0
    })
    res.setHeader("Access-Control-Allow-Origin", "*")
    res.setHeader("Access-Control-Allow-Credentials", "true")
    res.setHeader("Access-Control-Allow-Methods", "GET")
    res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers")
    const json = JSON.stringify({ key })
    res.setHeader('Content-Type', 'application/json;charset=utf8')
    res.setHeader('Content-Length', json.length)
    res.status(200)
    res.send(json)
})
app.get('/', (req, res) => {
    res.render('index', {
        code: typeof req.query.code === 'string' && req.query.code.startsWith('-') ? req.query.code : ''
    })
})
app.use(express.static(path.join(__dirname, 'public')))


const server = require('http').createServer(app)
const wss = new WebSocketServer({ server })

const reciveopmap = {
    'JOIN_GAME': 0,
    'AI_AUTH': 1,
    'MOVE': 2
}

const sendopmap = {

}
function* regexIter(regex, content) {
    let match
    while ((match = regex.exec(content)) != null)
        yield match
}

const createValueListener = client => snapshot => {
    const data = snapshot.val()
    if (data === null)
        return
    const history = []
    for (let [_, x, y] of regexIter(/\((\d),(\d)\)/g, data.history))
        history.push({ x, y })
    client.send(JSON.stringify({ op: 0, d: {
        event: 'BOARD_CHANGE',
        elements: value.elements.split(','),
        history,
        points: {
            light: value.points.light,
            dark: value.points.dark
        },
        turn: data.turn
    }}))
}

wss.on('connection', client => {
    let dbfn
    client.on('message', async message => {
        const {e: event, d: data } = JSON.parse(message)
        console.log({event, data})
        switch (event) {
            case reciveopmap.JOIN_GAME:
                firebase.database().ref('board').once('value', snapshot => {
                    if (snapshot.hasChild(data.code)) {
                        dbfn = createValueListener(client)
                        firebase.database().ref(`board/${data.code}`).on('value', dbfn)
                    }
                })
                break
            case reciveopmap.MOVE:
                const { ref, items, turn, piecesAround, history } = await checkPlacementValidity(data)
                    loop:for (let { i: direction } of piecesAround) {
                        let [cx, cy] = [data.x, data.y]
                        do {
                            // Increment the current x
                            switch(direction) { case 0:case 3:case 5:cx--;break ; case 2:case 4:case 7:cx++;break }
                            // Increment the current y
                            switch(direction) { case 0:case 1:case 2:cy--;break ; case 5:case 6:case 7:cy++;break }
                            if (items[indexFromPos(cx, cy)] === nextTurnChanges[turn % 2])
                                items[indexFromPos(cx, cy)] = nextTurnChanges[(turn + 1) % 2]
                            else continue loop
                        } while (true)
                    }

                    items[indexFromPos(data.x, data.y)] = nextTurnChanges[(turn + 1) % 2]

                    const points = { dark: 0, light: 0 }
                    for (let color of items)
                        if (color === 'black') points.dark++
                        else if (color === 'white') points.light++
                    
                    ref.set({
                        elements: items.join(','),
                        history: `${history}(${data.x},${data.y})`,
                        points,
                        turn: turn + 1
                    })
                    break
                case reciveopmap.AI_AUTH:break
        }
    })
})

const nextTurnChanges = ['white', 'black']

function checkPlacementValidity(query) {
    return new Promise((resolve, reject) => {
        const { code, x, y } = query
        // If parameters are missing 
        if (code === void 0 || x === void 0 || y === void 0) {
            const err = new Error(`Missing items in query ([${['code', 'x', 'y'].filter(item => item in query).join(', ')}])`)
            err.code = 400
            reject(err)
        }
        const ref = firebase.database().ref(`/board/${code}`)
        ref.once('value').then(snapshot => {
            const { elements, turn, history } = snapshot.val()
            const items = elements.split(',')
            if (items[indexFromPos(x, y)] !== 'none') {
                const err = new Error('You are not able to place the piece there')
                err.code = 400
                reject(err)
            }
            let piecesAround = getPicesAround(items, x, y).map((color, i) => ({ color, i }))
            piecesAround = piecesAround.filter(({ i: direction }) => {
                // Filtering edge directions
                if ((direction === 0 && x-1 === 0 && y-1 === 0) || // NW
                    (direction === 1 &&              y-1 === 0) || // N
                    (direction === 2 && x+1 === 9 && y-1 === 0) || // NE

                    (direction === 3 && x-1 === 0             ) || // W
                    (direction === 4 && x+1 === 9             ) || // E

                    (direction === 5 && x-1 === 0 && y+1 === 9) || // SW
                    (direction === 6 &&              y+1 === 9) || // S
                    (direction === 7 && x+1 === 0 && y+1 === 9))   // SE
                    return false
                return true
            })
            // Filter direction where there is no piece
            piecesAround = piecesAround.filter(({ color }) => color !== 'none')
            // Filtering out directions that have the same side as the player (dark - black tiles, light - white tiles)
            piecesAround = piecesAround.filter(({ color }) => color === nextTurnChanges[turn % 2])
            // Filter out direction that does not have a endpoint
            piecesAround = piecesAround.filter(({ color, i: direction }) => {
                let cy = y, cx = x
                let cc = color
                while(cc === color) {
                    // Increment the current x
                    switch(direction) { case 0:case 3:case 5:cx--;break ; case 2:case 4:case 7:cx++;break }
                    // Increment the current y
                    switch(direction) { case 0:case 1:case 2:cy--;break ; case 5:case 6:case 7:cy++;break }
                    // Check if current position is out of bounds
                    if (cx === 0 || cx === 9 ||
                        cy === 0 || cy === 9)
                        break
                    // Get new color
                    cc = items[indexFromPos(cx, cy)]
                }
                // If the current color is none return false
                if (cc === 'none') return false
                // Else if we did not find a different color in the current direction return false
                else if (cc === color) return false
                // Else we have hit a piece of the same color
                else return true
            })
            if (!piecesAround.length) {
                const err = new Error('You are not able to place the piece there')
                err.code = 403
                reject(err)
            }
            resolve({ ref, items, turn, piecesAround, history })
        })
    })
}

const indexFromPos = (x, y) => (y - 1) * 8 + x - 1

const getPicesAround = (items, x, y) => [
    [x - 1, y - 1], [x, y - 1], [x + 1, y - 1],
    [x - 1, y    ],             [x + 1, y    ],
    [x - 1, y + 1], [x, y + 1], [x + 1, y + 1]
].map(([x, y]) => indexFromPos(x, y)).map(pos => items[pos])

server.listen(parseInt(process.env.PORT) || 8080, () => {
    console.log('Server now listening on ' + server.address().port)
})